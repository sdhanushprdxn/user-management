const express = require('express');
const bodyParser = require('body-parser');
const uuidv1 = require('uuid/v1');
const fs = require('fs');
let user = require('./user_detail.json');
let history = require('./history.json');
let update = {}
const app = express();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

//signup new user...
app.post('/signup', (req,res,next) => {
  let hasMatch =false;
  for (let i = 0; i < user.length; ++i) {
    let username = user[i];
    if( username.email === req.body.email){
      hasMatch = true;
      break;
    }
  }
  if (hasMatch){
    res.send('User already Exists');
  }
  else{
    next();
  }
},(req,res)=> {
    let signupdata = req.body;
    signupdata['uuid'] = uuidv1();
    update[`${req.body.email}`]=`A new account was created with an emailId: ${req.body.email} at ${Date()}`;
    user.push(signupdata);
    history[0].created.push(update);

    Promise.all([
    fs.writeFile('user_detail.json',JSON.stringify(user,null,2), ()=>{
      console.log('New User Created');
    }),
    fs.writeFile('history.json',JSON.stringify(history,null,2), ()=>{
      console.log('Details Updated');
    })
    ])
    .then( res.send(`Successfully Created your account with emailID ${req.body.email}`));
})

//to authenticate user and return their uuid.
app.post('/login',(req,res,next) => {
  let hasMatch =false;
  for (let i = 0; i < user.length; ++i) {
    if((user[i].email === req.body.email) && (user[i].pass === req.body.pass)){
      hasMatch = true;
      break;
    }
  };
  
  if (!(hasMatch)){
    res.send('Incorrect email password combination');
  }
  else{
    return next();
  }
}, (req,res)=> {
  for (let i = 0; i < user.length; ++i) {
    if((user[i].email === req.body.email) && (user[i].pass === req.body.pass) ){
      return res.send(`Your UUID is ${user[i].uuid}`);
      // break;
    }
  };
});

// if user want to check every other user
app.get('/getusers/:uuid', (req,res)=> {
  let hasMatch = false;
  for (let i = 0; i < user.length; ++i) {
    if(user[i].uuid === req.params.uuid){
      hasMatch = true;
      break;
    }
  };
  if (hasMatch) {
    let userEmail = [];
    for (let i = 0; i < user.length; ++i) {
      userEmail.push(user[i].email);
    }
    res.send(userEmail);
  }
  else {
    res.send('Incorrrect UUID pin...')
  }
});

//del any user 

app.delete('/deleteSpecificUser/:uuid/:delEmail',(req,res) =>{
  let hasMatch = false;
  let notPresent = true;
  let person;
  let index = 0;
  for(let j =0; j < user.length; j++){
    if((user[j].email === req.params.delEmail)){
      notPresent =false;
      index = j;
      break;
    }
  };
  for (let i = 0; i < user.length; ++i) {
    if((user[i].uuid === req.params.uuid) ){
      hasMatch = true;
      person = user[i].email;
      break;
    }
  };

  if (notPresent) {
    return res.send('User not found');
  }
  else {
    if (hasMatch){
      update[`${req.params.delEmail}`]=`${req.params.delEmail} account was deleted by ${person} at ${Date()}`;
      user.splice(index,1);
      history[1].deleted.push(update);
      Promise.all([
        fs.writeFile('user_detail.json',JSON.stringify(user,null,2), ()=>{
          console.log('Account Deleted');
        }),
        fs.writeFile('history.json',JSON.stringify(history,null,2), ()=>{
          console.log('Details Updated');
        })
      ]).then(res.send(`Successfully Deleted The account with emailId ${req.params.delEmail}`))
    }
    else{
      return res.send(`Incorrect uuid of ${req.params.delEmail}`)
    }
  }
});

app.listen(1337 , ()=>{ console.log('Server started at 1337')})